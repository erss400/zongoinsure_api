<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Auth;
use Dingo\Api\Exception\ValidationHttpException;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
  use Helpers;

  protected function success($message)
  {
      return [
          'success' => [
              'status_code' => 200,
              'message' => $message
          ]
      ];
  }

  protected function throwValidationException(\Illuminate\Http\Request $request, $validator) {
      throw new ValidationHttpException($validator->errors());
  }

  /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ]);
    }

    public function is_youtube($youtube_link = '')
    {
        $video_id = '';
        //get the youtube video id
             $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
             $match;
             if (preg_match($regex_pattern, $youtube_link, $match)) {
                parse_str( parse_url( $youtube_link, PHP_URL_QUERY ), $my_array_of_vars );
                return true;
             } else {
                return false;
             }
    }

  public function is_email($email)
  {
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return true;
    } else {
      return false;
    }
  }
}
