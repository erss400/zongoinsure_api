<?php

namespace App\Http\Controllers\Api\V1;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\User\Password;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
* actions: r
*/
class UserController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	function __construct()
	{
		$this->middleware('checkClient', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
		$this->middleware('auth:api', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
	}

	public function register(Request $request)
	{
		# validate data
         $this->validate($request, [
            'name' => 'bail|required|string|min:6',
            'password' => 'bail|required|string|min:6',
            'username' => 'bail|required|unique:users',
            'email' => 'bail|required|email|unique:users',
            'account_type' => 'bail|required|string'
        ]);

        # save user object in database
        $user = User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'account_type' => $request->account_type,
            'is_activated' => 0,
        ]);

        # check if user created successfully
        if (!$user) {

            # return error if user creation failed
            return $this->error('unable to create account', 409);
        }

        $user->verification_code = mt_rand(193721, 666666);

        $user->save();

        # return true if user creation passed
        return $this->success('account created, check your email for verification email', 200);
	}

	public function verify(Request $request, $code = '')
	{
        if (empty($code)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No verification code found');
        }

		// find user with the code
        $user = User::where('verification_code', $code)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account found to match verification code');
        }

        $user->verification_code = null;

        $user->is_activated = 1;

        if (!$user->save()) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to verify user account');
        }

        return $this->success('User account has been verified, login to update your account settings', 200);
	}

	public function login(Request $request)
	{
        $this->validate($request, [
            'username' => 'bail|required|string',
            'password' => 'bail|required|min:6'
        ]);

        if ($this->is_email($request->username)) {
            $user = User::where('email', $request->username)->first();

            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User account NOT found');
            }

            if (!$user->is_activated) {
                throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Account has not been verified');
            }

            $credentials['password'] = $request->input('password');
            $credentials['email'] = $request->input('username');

            if (! $token = JWTAuth::attempt($credentials)) {
                throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Wrong credentials');
            }

            return $this->respondWithToken($token);
        }

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User account NOT found');
        }

        if (!$user->is_activated) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Account has not been verified');
        }

        $credentials = $request->all();

        if (! $token = JWTAuth::attempt($credentials)) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Wrong credentials');
        }

        return $this->respondWithToken($token);
	}

	public function me(Request $request)
	{
		
        $user = $request->user();

        switch ($user->account_type) {
            case 'company':
                return $this->response->item($user, (new UserTransformer)->setDefaultIncludes(['company']));
                break;
            
            default:
                return $this->response->item($user, (new UserTransformer)->setDefaultIncludes(['user']));
                break;
        }
	}

	public function forgot_password(Request $request)
	{
		$this->validate($request, [
			'username' => 'bail|required|string'
		]);

		$user = User::where('username', $request->username)->first();

		if (!$user) {
			return $this->error('no account is attach to the username', 404);
		}

		if ($this->is_email($user->username)) {
			$forgot = Password::create([
				'user_id' => $user->id,
				'token' => Hash::make($user->id . $user->username . mt_rand()),
			]);

			if (!$forgot) {
				return $this->error('unable to initaite the reset password process', 409);
			}

			return $this->success('reset password proccess initaited, check your email for the link', 200);
		}

		$forgot = Password::create([
				'user_id' => $user->id,
				'token' => mt_rand(193721, 999999)
			]);

			if (!$forgot) {
				return $this->error('unable to initaite the reset password process', 409);
			}

			return $this->success('reset password proccess initaited, check your phone for the code', 200);
	}

	public function check_reset_password_token(Request $request)
	{
		$this->validate($request, [
			'token' => 'bail|required|string'
		]);

		$reset = Password::where('token', $request->token)->first();

		if (!$reset) {
			return $this->error('token did not match any account', 404);
		}

		return $this->data($reset, 200);
	}

	public function reset_password(Request $request)
	{
		$this->validate($request, [
			'user_id' => 'bail|required|integer', 
			'password' => 'bail|required|string|confirmed'
		]);

		$user = User::where('id', $request->user_id)->first();

		if (!$user) {
			return $this->error('no user account found', 404);
		}

		$user->password = Hash::make($request->input('password'));

		if (!$user->save()) {
			return $this->error('something went wrong when trying to reset password', 409);
		}

		$reset_password = Password::where('user_id', $user->id)->first();

		$reset_password->delete();

		return $this->success('password reset successful', 200);
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'name' => 'bail|required|string|min:6',
		]);

		$user = $this->guard('auth:api')->user();

		$data = User::where('id', $user->id)->update([
			'name' => $request->name,
			'dob' => $request->date_of_birth,
			'pob' => $request->place_of_birth,
			'bio' => $request->bio,
		]);

		if (!$data) {
			return $this->error('unable to update account', 409);
		}

		return $this->success('account updated', 200);
	}

	public function update_picture(Request $request)
	{
		$this->validate($request, [
            'file' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $user = $this->guard('auth:api')->user();

        if (isset($request->file)) {
            // Storage::disk('public')->delete($user->picture);
            $picture_location = (isset($request->file)) ? $request->file->storeAs('users', str_slug($user->name) . '.' . $request->file('file')->getClientOriginalExtension(), 'public') : '';
        } else {
            $picture_location = $user->picture;
        }

        $result = User::where('id', $user->id)->update([
        	'picture' => $picture_location
        ]);

        if (!$result) {
            # error
            return $this->error('unable to update account picture', 409);
        }

        return $this->success('account picture updated', 200);
	}

	public function change_password(Request $request)
	{
		$this->validate($request, [
            'old_password' => 'bail|required|string',
            'password' => 'bail|required|string|min:6|confirmed'
        ]);

        $user = $this->guard('auth:api')->user();

        if (!Hash::check($request->old_password, $user->password)) {
           return $this->error('old password didn\'t match', 302); 
        }

        $result = User::where('id', $user->id)->update([
            'password' => Hash::make($request->password)
        ]);

        if (!$result) {
            # error
            return $this->error('unable to update account password', 302);
        }

        return $this->success('account password updated', 200);
	}

	public function logout(Request $request)
	{
		auth()->logout();

        return $this->success('Successfully logged out');
	}

	public function delete(Request $request)
	{
		# code...
	}
}