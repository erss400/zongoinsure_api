<?php

namespace App\Http\Controllers\Api\V1;

use Storage;
use Carbon\Carbon;
use App\Models\Consultant;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ConsultantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('checkConsultant', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
        $this->middleware('auth:api', ['except' => []]);
    }

    /**
     * Update.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'mission' => 'bail|required|string'
        ]);

        $updateConsultant = Consultant::where('user_id', $user->id)->update([
            'mission' => $request->mission,
        ]);

        if (!$updateConsultant) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account details');
    }
}
