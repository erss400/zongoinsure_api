<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Campaign;
use Carbon\Carbon;
use League\Fractal;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Models\Campaign\Video;
use App\Models\Campaign\Payout;
use App\Models\Campaign\Address;
use App\Models\Campaign\Partner;
use App\Models\Campaign\Picture;
use League\Fractal\Resource\Item;
use App\Http\Controllers\Controller;
use App\Transformers\CampaignTransformer;
use League\Fractal\Resource\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class CampaignController extends Controller
{
    /**
     * @var Manager
     */
    private $fractal;

    /**
     * @var CampaignTransformer
     */
    private $campaignTransformer;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Manager $fractal, CampaignTransformer $campaignTransformer)
    {
        $this->fractal = $fractal;
        $this->campaignTransformer = $campaignTransformer;
        $this->middleware('checkClient', ['except' => []]);
        $this->middleware('auth:api', ['except' => []]);
    }

    /**
     * Create campaign.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
    	$this->validate($request, [
            'category_id' => 'bail|required|integer',
            'title' => 'bail|required|string|unique:campaign_mysql.campaigns',
            'description' => 'bail|required|string',
        ]);

        $user = $this->guard('auth:api')->user();

        $campaign = New Campaign();
        $campaign->category_id = $request->category_id;
        $campaign->type = 'individual';
        $campaign->title = $request->title;
        $campaign->slug = str_slug($request->title);
        $campaign->description = $request->description;
        $result_campaign = $user->campaigns()->save($campaign);

        if (!$result_campaign) {
            # error
            return $this->error('campaign create error!!!', 302);
        }

        $resource = new Item($result_campaign, $this->campaignTransformer);

        // $this->fractal->parseIncludes(['address', 'payout', 'partners', 'picture', 'video']);

        $resource = $this->fractal->createData($resource);

        return $this->data($resource->toArray(), 200);

        // return $this->mix('new campaign created', $result_campaign, 200);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'campaign_id' => 'bail|required|integer',
            'target_amount' => 'bail|required|string',
            'end_date' => 'bail|required|string',
        ]);

        $campaign = Campaign::findOrFail($request->campaign_id);

        if (!$campaign) {
            return $this->error('resource not found', 404);
        }

        $campaign_result = Campaign::where('id', $campaign->id)->update([
            'target_amount' => $request->target_amount,
            'end_date' => $request->end_date,
            'afrogroup_launch' => ($request->afrogroup_launch == 'Yes') ? 1 : 0,
            'facebook_launch' => ($request->facebook_launch == 'Yes') ? 1 : 0,
            'instagram_launch' => ($request->instagram_launch == 'Yes') ? 1 : 0,
            'twitter_launch' => ($request->twitter_launch == 'Yes') ? 1 : 0,
        ]);

        // $payout = New Payout();

        // $payout->type = $request->account_type;
        // $payout->account_number = $request->account_number;

        if (!$campaign_result) {
            return $this->error('unable to update campaign data', 409);
        }

        return $this->success('campaign data updated', 200);
    }

    public function update_address(Request $request)
    {
        $this->validate($request, [
            'campaign_id' => 'bail|required|integer',
            'country_id' => 'bail|required|integer',
            'city_id' => 'bail|required|integer',
            'email' => 'bail|required|email'
        ]);

        $user = $this->guard('auth:api')->user();

        $campaign = Campaign::findOrFail($request->campaign_id);

        // instantiate and assign the campaign address information
        $address = New Address();
        $address->country_id = $request->country_id;
        $address->city_id = $request->city_id;
        $address->email = $request->email;

        // save campaign address
        $campaign->address()->save($address);

        if (!$campaign) {
            return $this->error('unable to update campaign address', 409);
        }

        return $this->success('campaign address updated', 200);
    }

    /**
     * Update campaign media
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_media(Request $request)
    {
    	$this->validate($request, [
    		'campaign_id' => 'bail|required|integer'
    	]);

        $user = $this->guard('auth:api')->user();
            
        $campaign = Campaign::findOrFail($request->campaign_id);

        if ($request->media_type == 'picture') {
            $this->validate($request, [
                'file' => 'bail|required|mimes:jpg,png,gif,jpeg'
            ]);

            $old_picture = Picture::where('campaign_id', $campaign->id)->first();

            $picture_location = (isset($request->file)) ? $request->file->storeAs('campaigns/pictures', str_slug($campaign->title) . '.' . $request->file('file')->getClientOriginalExtension(), 'public') : '';

            if (!$old_picture) {

            	$picture =  New Picture();

	            $picture->location = $picture_location;

	            $campaign->picture()->save($picture);

	            if (!$campaign) {
	                return $this->error('unable to add campaign picture', 409);
	            }

	            return $this->success('campaign picture added', 200);
            }

            $old_picture->location = $picture_location;

            $old_picture->save();

            if (!$old_picture) {
                return $this->error('unable to update campaign picture', 409);
            }

            return $this->success('campaign picture updated', 200);
        }

        $this->validate($request, [
            'file' => 'bail|required|mimes:mp4'
        ]);
                    
        $video_location = (isset($request->file)) ? $request->file->storeAs('campaigns/videos', str_slug($campaign->title) . '.' . $request->file('file')->getClientOriginalExtension(), 'public') : '';

        $old_video = Video::where('campaign_id')->first();

        if (!$old_video) {
        	$video =  New Video();

	        $video->location = $video_location;

	        $campaign->video()->save($video);

	        if (!$campaign) {
	            return $this->error('unable to add campaign video', 409);
	        }

	        return $this->success('campaign video added', 200);
	    }

        $old_video->location = $video_location;

        $old_video->save();

        if (!$old_video) {
            return $this->error('unable to update campaign video', 409);
        }

        return $this->success('campaign video updated', 200);
    }

    /**
     * Add campaign partners.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_partner(Request $request)
    {
        $this->validate($request, [
            'campaign_id' => 'bail|required|integer',
            'name' => 'bail|required|string',
            'email' => 'bail|required|string',
            'phone' => 'bail|required|string',
        ]);

        $campaign = Campaign::findOrFail($request->campaign_id);

        $partner = New Partner();

        $partner->name = $request->name;
        $partner->email = $request->email;
        $partner->phone_number = $request->phone;
        $result = $campaign->partners()->save($partner);

        if (!$result) {
            # error
            return $this->error('unable to add campaign partner', 409);
        }

        return $this->success('campaigne partner added', 200);

    }

    /**
     * Update campaign partners.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_partner(Request $request)
    {
        $this->validate($request, [
            'campaign_id' => 'bail|required|integer',
            'partner_id' => 'bail|required|integer',
            'name' => 'bail|required|string',
            'email' => 'bail|required|string',
            'phone' => 'bail|required|string',
        ]);

        $campaign = Campaign::findOrFail('camapaign_id');

        $partner = Partner::where('id', $request->partner_id)->where('camapaign_id', $campaign_id)->first();

        if (!$partner) {
            return $this->error('resource not found', 404);
        }

        $result = Partner::where('id', $request->partner_id)->where('camapaign_id', $campaign_id)->update([
        	'name' => $request->name,
        	'email' => $request->email,
        	'phone_number' => $request->phone_number
        ]);

        if (!$result) {
            # error
            return $this->error('unable to update campaign partner', 409);
        }

        return $this->success('campaign partner updated', 200);

    }

    /**
     * Remove campaign partners.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove_partner(Request $request)
    {
        $this->validate($request, [
            'campaign_id' => 'bail|required|integer',
            'partner_id' => 'bail|required|integer',
        ]);

        $campaign = Campaign::findOrFail($request->camapaign_id);

        $partner = Partner::where('id', $request->partner_id)->where('partnerable_id', $campaign_id)->first();

        if (!$partner) {
            return $this->error('resource not found', 404);
        }

        if (!$partner->delete()) {
            # error
            return $this->error('unable to delete campaign partner', 409);
        }

        return $this->success('campaign partner deleted', 200);

    }

    /**
     * Remove campaign
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
    	$this->validate($request, [
    		'campaign_id' => 'bail|required|integer'
    	]);

    	$campaign = Campaign::findOrFail($request->camapaign_id);

    	if (!$campaign) {
    		$this->error('resource not found', 404);
    	}

    	if ($campaign->delete()) {
    		# code...
    	}
    }
}
