<?php

namespace App\Http\Controllers\Api\V1;

use Storage;
use Carbon\Carbon;
use App\Models\User;
use App\Events\SignUpEvent;
use App\Models\User\Picture;
use Illuminate\Http\Request;
use App\Models\User\Password;
use App\Models\Location\Country;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Transformers\UserTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('checkClient', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
        $this->middleware('auth:api', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password', 'get_investors']]);
    }

    public function get_investors(Request $request, $pagination = 100)
    {
        $investors = User::paginate($pagination);

        return $this->response->paginator($investors, (new UserTransformer)->setDefaultIncludes(['country', 'city', 'state']));
    }

    public function register(Request $request)
    {
         $this->validate($request, [
            'country_id' => 'bail|required|integer',
            'state_id' => 'bail|required|integer',
            'city_id' => 'bail|required|integer',
            'account_type' => 'bail|required',
            'full_name' => 'bail|required|string|min:6|unique:users',
            'password' => 'bail|required|string|min:6',
            'phone' => 'bail|required',
            'username' => 'bail|required|string|unique:users',
            'email' => 'bail|required|string|email|unique:users',
        ]);

        $country = Country::findOrFail(231);

        $user = User::create([
            'country_id' => $request->country_id,
            'state_id' => $request->state_id,
            'city_id' => $request->city_id,
            'account_type' => $request->account_type,
            'full_name' => $request->input('full_name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'phone_number' => $request->phone,
            'currency' => $country->currency,
        ]);

        if (!$user) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to create user account');
        }

        $user->verification_code = mt_rand(193721, 666666);
        
        // $current_date = Carbon::now();
        // $user->expires_in = $current_date->addWeek(env('EXPIRES_IN'))->timestamp * 1000;

        $user->url = env('APP_EXTERNAL_URL') . '/verify?code=' . $user->verification_code;


        $user->save();

        // send mail
        // event(new SignUpEvent($user));

        # return true if user creation passed
        return $this->success('account created, check your email for verification code', 200);
    }

    public function verify($code='')
    {
        if (empty($code)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No verification code found');
        }

        $user = User::where('verification_code', $code)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account found to match verification code');
        }

        $user->verification_code = null;

        $user->is_activated = 1;

        if (!$user->save()) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to verify user account');
        }

        return $this->success('User account has been verified, login to update your account settings', 200);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'bail|required|string',
            'password' => 'bail|required|min:6'
        ]);

        if ($this->is_email($request->username)) {
            $user = User::where('email', $request->username)->first();

            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User account NOT found');
            }

            if (!$user->is_activated) {
                throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Account has not been verified');
            }

            $credentials['password'] = $request->input('password');
            $credentials['email'] = $request->input('username');

            if (! $token = JWTAuth::attempt($credentials)) {
                throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Wrong credentials');
            }

            return $this->respondWithToken($token);
        }

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User account NOT found');
        }

        if (!$user->is_activated) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Account has not been verified');
        }

        $credentials = $request->all();

        if (! $token = JWTAuth::attempt($credentials)) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Wrong credentials');
        }

        return $this->respondWithToken($token);
    }

    /**
     * forgot password
     *
     * @param  object Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot_password(Request $request)
    {
        $this->validate($request, [
            'username' => 'bail|required|string'
        ]);

        if ($this->is_email($request->username)) {
            $user = User::where('email', $request->username)->first();

            # check if user exist
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account with Email found');
            }

            # check if user already attempt forgot password process
            $old_forgot = Password::where('user_id', $user->id)->first();

            if ($old_forgot) {
                event(new ForgotPassword($old_forgot));

                return $this->success('Reset password proccess was initaited, check your email for the instructions', 200);
            }

            $forgot = Password::create([
                'user_id' => $user->id,
                'code' => mt_rand(193721, 999999),
            ]);

            if (!$forgot) {
                throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to initaite the reset password process');
            }

            $forgot->url = env('APP_EXTERNAL_DOMAIN') . 'reset-password?code=' . $forgot->code;

            $forgot->save();

            event(new ForgotPassword($forgot));

            return $this->success('Reset password proccess initaited, check your email for the instructions', 200);
        }

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No account with Username found');
        }

        # check if user already attempt forgot password process
        $old_forgot = Password::where('user_id', $user->id)->first();

        if ($old_forgot) {
            event(new ForgotPassword($old_forgot));

            return $this->success('Reset password proccess was initaited, check your email for the instructions', 200);
        }

        $forgot = Password::create([
            'user_id' => $user->id,
            'code' => mt_rand(193721, 999999),
        ]);

        if (!$forgot) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to initaite the reset password process');
        }

        $forgot->url = env('APP_EXTERNAL_DOMAIN') . 'reset-password?code=' . $forgot->code;

        $forgot->save();

        event(new ForgotPassword($forgot));

        return $this->success('reset password proccess initaited, check your email for the instructions');
    }

    public function verify_reset_code(Request $request)
    {
        $this->validate($request, [
            'code' => 'bail|required|string'
        ]);

        $reset = Password::where('code', $request->code)->first();

        if (!$reset) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Code did not match any account');
        }

        return $this->success('Enter new password');
    }

    public function reset_password(Request $request)
    {
        $this->validate($request, [
            'code' => 'bail|required', 
            'password' => 'bail|required|string|confirmed'
        ]);

        $reset_password = Password::where('code', $request->code)->first();

        $user = User::where('id', $reset_password->user_id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No user account was found');
        }

        $user->password = Hash::make($request->input('password'));

        if (!$user->save()) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Something went wrong when trying to reset password');
        }

        $reset_password->delete();

        event(new ResetPassword($user));

        return $this->success('Password resetted');
    }

    public function me(Request $request)
    {
        $user = $request->user();

        switch ($user->account_type) {
            case 'venture':
                return $this->response->item($user, (new UserTransformer)->setDefaultIncludes(['venture', 'country', 'state', 'city']));
                break;

            case 'investor':
                return $this->response->item($user, (new UserTransformer)->setDefaultIncludes(['investor', 'country', 'state', 'city']));
                break;

            case 'consultant':
                return $this->response->item($user, (new UserTransformer)->setDefaultIncludes(['consultant', 'country', 'state', 'city']));
                break;
            
            default:
                return $this->response->item($user, (new UserTransformer)->setDefaultIncludes(['client', 'country', 'state', 'city']));
                break;
        }
    }

    public function secure_account(Request $request)
    {
        $this->validate($request, [
            'password' => 'bail|required|string'
        ]);

        $user = $request->user();

        if ($user->username !== $request->username) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Unauthorized user');
        }

        if (!Hash::check($request->password, $user->password)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Password is incorrect');
        }

        return $this->success('Your cashout is secure, continue');
    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'bail|required|string',
            'password' => 'bail|required|string|min:6|confirmed'
        ]);

        $user = $this->auth->user();

        if (!Hash::check($request->old_password, $user->password)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Old password didn\'t match');
        }

        $result = User::where('id', $user->id)->update([
            'password' => Hash::make($request->password)
        ]);

        if (!$result) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to change password user account');
        }

        return $this->success('Account password has been changed, we will log you out so you can login with new password');
    }

    public function change_username(Request $request)
    {
        $this->validate($request, [
            'username' => 'bail|required|string|unique:users'
        ]);

        $user = $this->auth->user();

        $user->username = $request->username;

        if (!$user) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to change account username');
        }

        // send email
        return $this->success('Account username changed');
    }

    /**
     * Update.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'name' => 'bail|required|string',
            'phone_number' => 'bail|required|string',
            // 'currency' => 'bail|required|string',
            'description' => 'bail|required|string'
        ]);

        $updateUser = User::where('id', $user->id)->update([
            'full_name' => $request->name,
            'phone_number' => $request->phone_number,
            // 'currency' => $request->currency,
            'description' => $request->description
        ]);

        if (!$updateUser) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account details');
    }

    /**
     * Update Media.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_media(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Operation not allowed');
        }

        $this->validate($request, [
            'media' => 'required' 
        ]);

        if ($this->is_youtube($request->media)) {

            $user->media = $request->media;
            $user->media_type = 'video';

            if (!$user->save()) {
                throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update account media');
            }

            return $this->success('Updated account media');
        }

        $this->validate($request, [
            'media' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $picture_location = (isset($request->media)) ? $request->media->storeAs('users', str_slug($user->username) . '.jpg', 'public') : '';

        $user->media = $picture_location;
        $user->media_type = 'picture';

        if (!$user->save()) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update account media');
        }

        return $this->success('Updated account media');
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return $this->success('Successfully logged out');
    }
}