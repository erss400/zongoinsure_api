<?php

namespace App\Http\Controllers\Api\V1;

use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User\Investor;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InvestorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('checkInvestor', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
        $this->middleware('auth:api', ['except' => []]);
    }

    /**
     * Update.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'mailing_address' => 'bail|required|string',
            'identification_doc' => 'bail|required|mimes:jpg,png,gif,jpeg',
            // 'identification_number' => 'bail|required|string'
            // 'social_security_number' => 'bail|required|string',
        ]);

        $investor = Investor::where('user_id', $user->id)->first();

        if (!$investor) {

            $picture_location = ($request->identification_doc !== 'null') ? $request->identification_doc->storeAs('investors/ids', str_slug($user->username) . '_id.jpg', 'public') : '';

            $createInvestor = Investor::create([
                'user_id' => $user->id,
                'mailing_address' => $request->mailing_address,
                'identification_doc' => $picture_location,
                'social_security_number' => $request->social_security_number,
                'identification_number' => $request->identification_number,
            ]);


            if (!$saveVenture) {
                throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
            }

            return $this->success('Updated investor account details');
        }

        $picture_location = ($request->identification_doc !== 'null') ? $request->identification_doc->storeAs('investors/ids', str_slug($user->username) . '_id.jpg', 'public') : $investor->identification_doc;

         $updateInvestor = Investor::where('user_id', $user->id)->update([
            'mailing_address' => $request->mailing_address,
            'identification_doc' => $picture_location,
            'social_security_number' => $request->social_security_number,
            'identification_number' => $request->identification_number,
        ]);

        if (!$updateInvestor) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account details');
    }

    /**
     * Update Money Account.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_basic(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'payment_method' => 'bail|required|string',
        ]);

        $updateInvestor = Investor::where('user_id', $user->id)->update([
            'payment_method' => $request->payment_method,
            'mobile_money_provider' => $request->mobile_money_provider,
            'mobile_money_number' => $request->mobile_money_number,
            'bank_name' => $request->bank_name,
            'bank_statement' => $request->bank_statement,
            'credit_card_token' => $request->credit_card_token,
        ]);

        if (!$updateInvestor) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account details');
    }
}
