<?php

namespace App\Http\Controllers\Api\V1;

use Storage;
use Carbon\Carbon;
use App\Models\User\Team;
use Illuminate\Http\Request;
use App\Models\User\Picture;
use App\Models\User\Venture;
use App\Models\User\Cashout;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Models\User\Venture\Market;
use App\Models\User\Venture\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User\Venture\Support;
use App\Models\User\Venture\Investment;
use App\Models\User\Venture\Comment\Reply;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\User\CashoutTransformer;
use App\Transformers\User\VentureTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VentureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('checkClient', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
        $this->middleware('auth:api', ['except' => ['get', 'support', 'post_comment']]);
    }

    /**
     * Get
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        if ($request->exists('category_id')) {
            if (!empty($request->category_id)) {
                $paginator = Venture::where('category_id', $request->category_id)->where('active', 1)->paginate(100);
            } else {
                $paginator = Venture::paginate(100);
            }
        } else {
            $paginator = Venture::paginate(100);
        }
        
        return $this->response->paginator($paginator, (new VentureTransformer)->setDefaultIncludes(['market', 'teams', 'pictures', 'user', 'comments']));
    }

    /**
     * Update Basic.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_basic(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'investment_proposal' => 'required|string|min:20',
            'target_amount' => 'required|string|',
            'start_date' => 'bail|required',
            'end_date' => 'bail|required',
            'amount_per_share' => 'bail|required',
        ]);

        if (!$request->has('venture_id')) {
            $saveVenture = Venture::create([
                'user_id' => $user->id,
                'investment_proposal' => $request->investment_proposal,
                'target_amount' => $request->target_amount,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'amount_per_share' => $request->amount_per_share,
                'code' => random_int(1111, 999999)
            ]);

            if (!$saveVenture) {
                throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
            }

            return $this->success('Added venture account details');
        }

        $updateVenture = Venture::where('user_id', $user->id)->where('id', $request->venture_id)->update([
            'investment_proposal' => $request->investment_proposal,
            'target_amount' => $request->target_amount,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'amount_per_share' => $request->amount_per_share
        ]);

        if (!$updateVenture) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated venture account details');
    }

    /**
     * Update Advance.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_advance(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'return_on_investment' => 'required|string|min:30',
            'risk_analysis' => 'required|string|min:20',
        ]);

        if (!$request->has('venture_id')) {
            $updateVenture = Venture::create([
                'user_id' => $user->id,
                'return_on_investment' => $request->return_on_investment,
                'risk_analysis' => $request->risk_analysis,
                'code' => random_int(1111, 999999)
            ]);

            if (!$updateVenture) {
                throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
            }

            return $this->success('Added venture account details');
        }

        $updateVenture = Venture::where('user_id', $user->id)->where('id', $request->venture_id)->update([
            'return_on_investment' => $request->return_on_investment,
            'risk_analysis' => $request->risk_analysis,
        ]);

        if (!$updateVenture) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated venture account details');
    }

    /**
     * Update Tax Card.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function update_tax_card(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Operation not allowed');
        }

        $this->validate($request, [
            'media' => 'bail|required|mimes:pdf'
        ]);

        $picture_location = (isset($request->media)) ? $request->media->storeAs('ventures/tax_cards', str_slug($user->username) . '.pdf', 'public') : '';

        $venture = Venture::where('user_id', $user->id)->update([
            'tax_card' => $picture_location
        ]);

        if (!$venture) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update account media');
        }

        return $this->success('Updated account tax card');    
    }

    /**
     * Update Company Registration Number.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function update_company_registration(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Operation not allowed');
        }

        $this->validate($request, [
            'media' => 'bail|required|mimes:pdf'
        ]);

        $picture_location = (isset($request->media)) ? $request->media->storeAs('ventures/registrations', str_slug($user->username) . '.pdf', 'public') : '';

        $venture = Venture::where('user_id', $user->id)->update([
            'company_registration' => $picture_location
        ]);

        if (!$venture) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update account media');
        }

        return $this->success('Updated account registration data');    
    }

    /**
     * Add Market.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_market(Request $request, $venture_id = '')
    {
       $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'target_market' => 'bail|required|string|min:20',
            'value_proposition' => 'bail|required|string|min:20',
            'products' => 'bail|required|string|min:20'
        ]);

        // save the market information
        $market = Market::create([
            'venture_id' => $venture_id,
            'target_market' => $request->target_market,
            'value_proposition' => $request->value_proposition,
            'products' => $request->products,
            'revenue_streams' => $request->revenue_streams
        ]);

        
        if (!$market) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Added account market details');
    }

    /**
     * Update Market.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_market(Request $request, $venture_id = '', $market_id = '')
    {
       $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $market = Market::where('venture_id', $venture_id)->where('id', $market_id)->first();

        if (!$market) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Unauthorized access to resource');
        }

        $this->validate($request, [
            'target_market' => 'bail|required|string|min:20',
            'value_proposition' => 'bail|required|string|min:20',
            'products' => 'bail|required|string|min:20'
        ]);

        // update the market information
        $updateMarket = Market::where('id', $market_id)->update([
            'venture_id' => $venture_id,
            'target_market' => $request->target_market,
            'value_proposition' => $request->value_proposition,
            'products' => $request->products,
            'revenue_streams' => $request->revenue_streams
        ]);

        
        if (!$updateMarket) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account market details');
    }

    /**
     * Update Execution.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_execution(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'use_of_funds' => 'bail|required|string|min:20',
        ]);

        // update the executive information
        $updateVenture = Venture::where('user_id', $user->id)->update([
            'use_of_funds' => $request->use_of_funds,
            'company_traction' => $request->company_traction,
            'previous_funds' => $request->previous_funds,
            'partnerships' => $request->partnerships,
            'awards' => $request->awards
        ]);

        
        if (!$updateVenture) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account executive details');
    }

    /**
     * Add Team Member.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_team(Request $request, $venture_id = '')
    {

        $this->validate($request, [
            'name' => 'bail|required|string',
            'title' => 'bail|required|string',
            'description' => 'bail|required|string',
            'media' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Unauthorized user');
        }

        $venture = Venture::where('id', $venture_id)->where('user_id', $user->id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Operation not allowed');
        }

        $picture_location = (isset($request->media)) ? $request->media->storeAs('users/teams', str_slug($request->name) . '.jpg', 'public') : '';

        $team = new Team();
        $team->name = $request->name;
        $team->title = $request->title;
        $team->description = $request->description;
        $team->picture = $picture_location;

        $saveTeam = $venture->teams()->save($team);

        if (!$saveTeam) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to add team member');
        }

        return $this->success('Added team member');

    }

    /**
     * Update Team Member.
     * @param \Illuminate\Http\Request $request, $team_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_team(Request $request, $venture_id = '', $team_id = '')
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException('Unauthorized user');
        }

        $venture = Venture::where('id', $venture_id)->where('user_id', $user->id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User does not exist');
        }

        $team = Team::where('id', $team_id)->where('teamable_id', $venture_id)->first();

        if (!$team) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Team member not found');
        } 

        $this->validate($request, [
            'name' => 'bail|required|string',
            'title' => 'bail|required|string',
            'description' => 'bail|required|string',
        ]);

        $picture_location = ($request->media !== 'null') ? $request->media->storeAs('users/teams', str_slug($request->name) . '.jpg', 'public') : $team->picture;

        $result = Team::where('id', $team->id)->update([
            'name' => $request->name,
            'title' => $request->title,
            'description' => $request->description,
            'picture' => $picture_location,
        ]);

        if (!$result) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update team member');
        }

        return $this->success('Updated team member');

    }

    /**
     * Remove Team Member.
     * @param  $team_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove_team(Request $request, $venture_id = '', $team_id = '')
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException('Unauthorized user');
        }

        $venture = Venture::where('id', $venture_id)->where('user_id', $user->id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User does not exist');
        }

        $team = Team::where('id', $team_id)->where('teamable_id', $venture_id)->first();

        if (!$team) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Team member not found');
        }

        $image_file = $team->picture;

        if (!$team->delete()) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to delete account team member');
        }

        // delete picture from storage
        Storage::disk('public')->delete($image_file);
        return $this->success('Deleted team member');

    }

    /**
     * Add Picture.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_picture(Request $request, $venture_id = '')
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Unauthorized user');
        }

        $venture = Venture::where('id', $venture_id)->where('user_id', $user->id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User does not exist');
        }

        $this->validate($request, [
            'title' => 'bail|required|string|unique:pictures',
            'media' => 'bail|required|mimes:jpg,png,gif,jpeg'
        ]);

        $picture_location = (isset($request->media)) ? $request->media->storeAs('users/pictures', str_slug($request->title) . '.jpg', 'public') : '';
        
        $picture = new Picture();
        $picture->title = $request->title;
        $picture->picture = $picture_location;
        $savePicture = $venture->pictures()->save($picture);

        if (!$savePicture) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to add account picture');
        }

        return $this->success('Added account picture');
    }

    /**
     * Remove Picture.
     * @param \Illuminate\Http\Request $request $picture_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove_picture(Request $request, $venture_id = '', $picture_id = '')
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Unauthorized user');
        }

        $venture = Venture::where('id', $venture_id)->where('user_id', $user->id)->first();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User does not exist');
        }

        $picture = Picture::where('id', $picture_id)->where('pictureable_id', $venture_id)->first();

        if (!$picture) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Account picture not found');
        }

        $image_file = $picture->picture;

        if (!$picture->delete()) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to delete account picture');
        }

        // delete picture from storage
        Storage::disk('public')->delete($image_file);
        return $this->success('Deleted picture');
    }

    /**
     * Support
     * @param \Illuminate\Http\Request $request, $venture_id
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function support(Request $request, $venture_id = '')
    {
        $venture = Venture::where('id', $venture_id)->first();

        if (!$venture) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Venture resource not found');
        }

        $support = Support::create([
            'venture_id' => $venture_id,
            'name' => $request->name,
            'email' => $request->email,
            'amount' => $request->amount
        ]);

        if (!$support) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not success');
        }

        $venture->donated_amount += $request->amount;
        $venture->save();

        return $this->success('Thank You For Your Support');
    }

    /**
     * Invest With US
     * @param \Illuminate\Http\Request $request, $venture_id
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function invest_with_us(Request $request, $venture_id = '')
    {
        $user = $request->user();

        $venture = Venture::where('id', $venture_id)->first();

        $existInvestment = Investment::where('user_id', $user->id)->where('venture_id', $venture_id)->first();

        if ($existInvestment) {
            return $this->success('You have already invested on these venture');
        }

        if (!$venture) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Venture resource not found');
        }

        $investment = Investment::create([
            'venture_id' => $venture_id,
            'user_id' => $user->id,
            'amount_per_share' => $request->amount_per_share,
            'number_of_shares' => $request->number_of_shares,
            'amount_invested' => $request->amount_invested
        ]);

        if (!$investment) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not success');
        }

        $venture->donated_amount += $request->amount_invested;
        $venture->save();

        return $this->success('Thank You For Investing');
    }

    /**
     * Initiate Venture Cashout
     * @param \Illuminate\Http\Request $request, $venture_id
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function initiate_cashout(Request $request, $venture_id = '')
    {
        $this->validate($request, [
            'account_type' => 'bail|required|string',
            'account_number' => 'bail|required_if:account_type,cc,momo,bank',
            'name' => 'bail|required_if:account_type,ex,wu',
            'phone_number' => 'bail|required_if:account_type,ex,wu',
            'country' => 'bail|required_if:account_type,ex,wu',
            'city' => 'bail|required_if:account_type,ex,wu',
        ]);

        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException('Unauthorized access');
        }

        $venture = Venture::where('user_id', $user->id)->where('id', $venture_id)->first();

        if (!$venture) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'Venture does not belong to user account');
        }

        $exit_cashout = Cashout::where('user_id', $user_id)->where('venture_id', $venture_id)->first();
        if ($exit_cashout) {
            return $this->success('Cashout proccess is ' . $exit_cashout->status);
        }

        $cashout = Cashout::create([
            'user_id' => $user_id,
            'venture_id' => $venture_id,
            'full_name' => $request->name,
            'account_type' => $request->account_type,
            'account_number' => $request->account_number,
            'phone_number' => $request->phone_number,
            'country' => $request->country,
            'city' => $request->city,
            'status' => 'initiated',
        ]);

        $venture->is_live = 0;
        $venture->save();

        if (!$cashout) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to initiate cashout proccess venture');
        }

        return $this->success('Cashout proccess initiated');
    }

    /**
     * Get User Cashouts
     * @param \Illuminate\Http\Request $request, $venture_id
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function get_user_cashouts(Request $request)
    {
        $user = $request->user();

        $cashouts = Cashout::where('user_id', $user->id)->get();

        return $this->response->collection($cashouts, new CashoutTransformer);
    }

    /**
     * Comment on venture
     * @param \Illuminate\Http\Request $request, $venture_id
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function post_comment(Request $request, $venture_id = '')
    {
        $venture = Venture::where('id', $venture_id)->first();

        if (!$venture) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Venture resource not found');
        }

        $comment = Comment::create([
            'venture_id' => $venture_id,
            'name' => $request->name,
            'message' => $request->message
        ]);

        if (!$comment) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not success');
        }

        return $this->success('you commented on the venture');
    }

    /**
     * Reply On Comment
     * @param \Illuminate\Http\Request $request, $comment_id
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function post_comment_reply(Request $request, $comment_id = '')
    {
        $comment = Comment::where('id', $comment_id)->first();

        if (!$comment) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Comment resource not found');
        }

        $reply = Reply::create([
            'comment_id' => $comment_id,
            'message' => $request->message
        ]);

        if (!$reply) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not success');
        }

        return $this->success('you replied to the comment');
    }
}
