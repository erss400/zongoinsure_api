<?php

namespace App\Http\Controllers\Api\V1;

use Carbon\Carbon;
use App\Models\User\Company;
use App\Mail\CompanyWelcome;
use Illuminate\Http\Request;
use App\Models\User\Company\Team;
use App\Models\User\Company\About;
use Illuminate\Support\Facades\DB;
use App\Models\User\Company\Office;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Models\User\Company\Service;
use App\Transformers\User\CompanyTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CompanyController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	function __construct()
	{
		$this->middleware('auth:api', ['except' => []]);
	}

	public function get_started(Request $request, $user_id = '')
	{
		$this->validate($request, [
			'category_id' => 'bail|required|integer',
			'company_name' => 'bail|required|string|min:6',
			'tax_id_number' => 'bail|required|string|min:8',
			'registration_number' => 'bail|required|string|min:10',
			'date_of_incorporation' => 'bail|required|string',
			'structure_type' => 'bail|required|string',
		]);

		$data = Company::create([
			'user_id' => $user_id,
			'category_id' => $request->category_id,
			'company_name' => $request->company_name,
			'tax_id_number' => $request->tax_id_number,
			'registration_number' => $request->registration_number,
			'date_of_incorporation' => $request->date_of_incorporation,
			'structure_type' => $request->structure_type,
		]);

		if (!$data) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable add company data');
		}

		// Mail::to($data)->send(new CompanyWelcome($data));

		return $this->success('Added company data');

	}

	public function update(Request $request, $user_id = '')
	{
		$this->validate($request, [
			'category_id' => 'bail|required|integer',
			'company_name' => 'bail|required|string|min:6',
			'tax_id_number' => 'bail|required|string|min:8',
			'registration_number' => 'bail|required|string|min:10',
			'date_of_incorporation' => 'bail|required|string',
			'first_name' => 'bail|required|string|min:6',
			'last_name' => 'bail|required|string|min:6',
			'email' => 'bail|required|string|email',
			'phone_number' => 'bail|required|string',
			'structure_type' => 'bail|required|string',
		]);

		$data = Company::where('id', $user->id)->update([
			'category_id' => $request->category_id,
			'company_name' => $request->company_name,
			'tax_id_number' => $request->tax_id_number,
			'registration_number' => $request->registration_number,
			'date_of_incorporation' => $request->date_of_incorporation,
			'structure_type' => $request->structure_type,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'phone_number' => $request->phone_number,
		]);

		if (!$data) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update company account');
		}

		return $this->success('Updated company account');
	}

	public function add_about(Request $request, $company_id = '')
	{
		$this->validate($request, [
			'country_id' => 'bail|required|integer',
			'state_id' => 'bail|required|integer',
			'city_id' => 'bail|required|integer',
			'street' => 'bail|required|string',
			'email' => 'bail|required|string|email',
			'phone_number' => 'bail|required|string',
			'about_us' => 'bail|required|string',
			'mission_statement' => 'bail|required|string',
			'mission' => 'bail|required|string',
			'vision' => 'bail|required|string'
		]);

		$result = About::create([
			'country_id' => $request->country_id,
			'state_id' => $request->state_id,
			'city_id' => $request->city_id,
			'street' => $request->street,
			'company_id' => $company_id,
			'email' => $request->email,
			'phone_number' => $request->phone_number,
			'about_us' => $request->about_us,
			'mission_statement' => $request->mission_statement,
			'mission' => $request->mission,
			'vision' => $request->vision,
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable update company about information');
		}

		return $this->success('Added company about information');
	}

	public function update_about(Request $request, $company_id = '', $about_id = '')
	{
		$this->validate($request, [
			'country_id' => 'bail|required|integer',
			'state_id' => 'bail|required|integer',
			'city_id' => 'bail|required|integer',
			'street' => 'bail|required|string',
			'email' => 'bail|required|string|email',
			'phone_number' => 'bail|required|string',
			'about_us' => 'bail|required|string',
			'mission_statement' => 'bail|required|string',
			'mission' => 'bail|required|string',
			'vision' => 'bail|required|string'
		]);

		$about = About::findOrFail($about_id);

		if (!$about) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('About resource not found');
		}

		$result = About::where('id', $about_id)->update([
			'country_id' => $request->country_id,
			'state_id' => $request->state_id,
			'city_id' => $request->city_id,
			'street' => $request->street,
			'email' => $request->email,
			'phone_number' => $request->phone_number,
			'about_us' => $request->about_us,
			'mission_statement' => $request->mission_statement,
			'mission' => $request->mission,
			'vision' => $request->vision,
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable update company about information');
		}

		return $this->success('Updated company about information');
	}

	public function update_media(Request $request, $company_id = '')
	{
		$this->validate($request, [
			'logo' => 'bail|required|mimes:jpg,png,gif,jpeg',
			'banner' => 'bail|required|mimes:jpg,png,gif,jpeg'
		]);

		$company = Company::findOrFail($company_id);

		$logo_location = (isset($request->logo)) ? $request->logo->storeAs('companies', str_slug($company->company_name) . '_logo' . '.' . $request->file('logo')->getClientOriginalExtension(), 'public') : '';

		$banner_location = (isset($request->banner)) ? $request->banner->storeAs('companies', str_slug($company->company_name) . '_banner' . '.' . $request->file('banner')->getClientOriginalExtension(), 'public') : '';

		$result = Company::where('id', $company_id)->update([
			'logo' => $logo_location,
			'banner' => $banner_location
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable update company media information');
		}

		return $this->success('Updated company media');
	}

	public function add_service(Request $request, $company_id = '')
	{
		$this->validate($request, [
			'title' => 'bail|required|string',
			'content' => 'bail|required|string',
			'policy' => 'bail|required|string',
			'picture' => 'bail|required|mimes:jpg,png,gif,jpeg'
		]);

		$company = Company::findOrFail($company_id);

		$picture_location = (isset($request->picture)) ? $request->picture->storeAs('services', str_slug($company->company_name) . '_' . str_slug($request->title) . '.' . $request->file('picture')->getClientOriginalExtension(), 'public') : '';

		$result = Service::create([
			'company_id' => $company->id,
			'title' => $request->title,
			'content' => $request->content,
			'policy' => $request->policy,
			'picture' => $picture_location
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to add company service');
		}

		return $this->success('Added new service');
	}

	public function update_service(Request $request, $company_id = '', $service_id = '')
	{
		$this->validate($request, [
			'title' => 'bail|required|string',
			'content' => 'bail|required|string',
			'policy' => 'bail|required|string',
		]);

		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Company resource not found');
		}

		$service = Service::where('id', $service_id)->where('company_id', $company_id)->first();

		if (!$service) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Service resource not found');
		}

		$picture_location = (isset($request->picture)) ? $request->picture->storeAs('services/' . str_slug($company->company_name), str_slug($request->title) . '.' . $request->file('picture')->getClientOriginalExtension(), 'public') : $service->picture;

		$result = Service::where('id', $service_id)->update([
			'company_id' => $company->id,
			'title' => $request->title,
			'content' => $request->content,
			'policy' => $request->policy,
			'picture' => $picture_location
		]);

		if (!$result) {

		$company = Company::findOrFail($company_id);
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update company service');
		}

		return $this->success('Updated company service');
	}

	public function delete_service(Request $request, $company_id = '', $service_id = '')
	{
		$service = Service::where('id', $service_id)->where('company_id', $company_id)->first();

		if (!$service) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Service resource not found');
		}

		if (!$service->delete()) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Unable to delete company service');
		}

		return $this->success('Deleted company service');
	}

	public function add_office(Request $request, $company_id = '')
	{
		$this->validate($request, [
			'country_id' => 'bail|required|integer',
			'state_id' => 'bail|required|integer',
			'city_id' => 'bail|required|integer',
			'street' => 'bail|required|string',
			'email' => 'bail|required|email',
			'phone_number' => 'bail|required|string',
		]);

		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Company resource not found');
		}

		$result = Office::create([
			'company_id' => $company_id,
			'country_id' => $request->country_id,
			'state_id' => $request->state_id,
			'city_id' => $request->city_id,
			'street' => $request->street,
			'email' => $request->email,
			'phone_number' => $request->phone_number,
			'longitude' => $request->longitude,
			'latitude' => $request->latitude
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to add branch office');
		}

		return $this->success('Added branch office');
	}

	public function update_office(Request $request, $company_id = '', $office_id = '')
	{
		$this->validate($request, [
			'country_id' => 'bail|required|integer',
			'state_id' => 'bail|required|integer',
			'city_id' => 'bail|required|integer',
			'street' => 'bail|required|string',
			'email' => 'bail|required|email',
			'phone_number' => 'bail|required|string',
		]);

		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Company resource not found');
		}

		$office = Office::where('id', $office_id)->where('company_id', $company_id)->first();

		if (!$office) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Branch office resource not found');
		}

		$result = Office::where('id', $office->id)->update([
			'country_id' => $request->country_id,
			'state_id' => $request->state_id,
			'city_id' => $request->city_id,
			'street' => $request->street,
			'email' => $request->email,
			'phone_number' => $request->phone_number,
			'longitude' => $request->longitude,
			'latitude' => $request->latitude
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update branch office');
		}

		return $this->success('Updated branch office');
	}

	public function delete_office(Request $request, $company_id = '', $office_id = '')
	{
		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Company resource not found');
		}

		$office = Office::where('id', $office_id)->where('company_id', $company_id)->first();

		if (!$office) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Branch office resource not found');
		}

		if (!$office->delete()) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to delete branch office');
		}

		return $this->success('Deleted branch office');
	}

	public function add_team(Request $request, $company_id = '')
	{
		$this->validate($request, [
			'name' => 'bail|required|string',
			'position' => 'bail|required|string',
			'bio' => 'bail|required|string',
			'picture' => 'bail|required|mimes:jpg,png,gif,jpeg'
		]);

		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No company found');
		}

		$picture_location = (isset($request->picture)) ? $request->picture->storeAs('teams/' . str_slug($company->company_name), str_slug($request->name) . '.' . $request->file('picture')->getClientOriginalExtension(), 'public') : '';

		$result = Team::create([
			'company_id' => $company->id,
			'name' => $request->name,
			'position' => $request->position,
			'bio' => $request->bio,
			'picture' => $picture_location,
			// 'facebook' => $request->facebook,
			// 'twitter' => $request->twitter,
			// 'instagram' => $request->instagram,
			// 'linkedin' => $request->linkedin
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to add team member');
		}

		return $this->success('Added team member');
	}

	public function update_team(Request $request, $company_id = '', $team_id = '')
	{
		$this->validate($request, [
			'name' => 'bail|required|string',
			'position' => 'bail|required|string',
			'bio' => 'bail|required|string',
		]);

		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Company resource not found');
		}

		$team = Team::where('id', $team_id)->where('company_id', $company_id)->first();

		if (!$team) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Team memner not found');
		}

		$picture_location = (isset($request->picture)) ? $request->picture->storeAs('teams/' . str_slug($company->company_name), str_slug($request->name) . '.' . $request->file('picture')->getClientOriginalExtension(), 'public') : $team->picture;

		$result = Team::where('id', $team->id)->update([
			'name' => $request->name,
			'position' => $request->position,
			'bio' => $request->bio,
			'picture' => $picture_location,
			'facebook' => $request->facebook,
			'twitter' => $request->twitter,
			'instagram' => $request->instagram,
			'linkedin' => $request->linkedin
		]);

		if (!$result) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to update team member');
		}

		return $this->success('Updated team member');
	}

	public function delete_team(Request $request, $company_id = '', $team_id = '')
	{
		$company = Company::findOrFail($company_id);

		if (!$company) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Company resource not found');
		}

		$team = Team::where('id', $team_id)->where('company_id', $company_id)->first();

		if (!$team) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Team member not found');
		}

		if (!$team->delete()) {
			throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to delete team member');
		}

		return $this->success('Deleted team member');
	}
    
}
