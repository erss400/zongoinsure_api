<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Http\Controllers\Controller;
use App\Transformers\CityTransformer;
use App\Transformers\StateTransformer;
use App\Transformers\CountryTransformer;
use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SearchController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function search_country(Request $request)
    {
        $q = $request->input('q', 'Cameroon');

        $searchResult = Country::where('name', 'like', '%'. $q . '%')->take(10)->get();

        // Get transformed array of data
        return $this->response->collection($searchResult, new CountryTransformer);
    }

    public function search_state(Request $request)
    {
        $q = $request->input('q', 'Buea');

        $country_id = $request->input('country_id', '0');

        $searchResult = State::where('name', 'like', '%'. $q . '%')->where('country_id', $country_id)->take(10)->get();

        return $this->response->collection($searchResult, new StateTransformer); // Get transformed array of data
    }

    public function search_city(Request $request)
    {
        $q = $request->input('q', 'Buea');

        if ($request->exists('state_id')) {
            $state_id = $request->input('state_id', '0');
            $searchResult = City::where('name', 'like', '%'. $q . '%')->where('state_id', $state_id)->take(10)->get();
        } else {
            $searchResult = City::where('name', 'like', '%'. $q . '%')->take(10)->get();
        }

        return $this->response->collection($searchResult, new CityTransformer); // Get transformed array of data
    }

    public function search_category(Request $request)
    {
        $q = $request->input('q', 'Agricultural Insurance‎');

        $searchResult = Category::where('title', 'like', '%'. $q . '%')->take(10)->get();

        // Get transformed array of data
        return $this->response->collection($searchResult, new CategoryTransformer);
    }
}
