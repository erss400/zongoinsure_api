<?php

namespace App\Transformers\User;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\User\Company;
use League\Fractal\TransformerAbstract;
use App\Transformers\CategoryTransformer;
use App\Transformers\User\Company\TeamTransformer;
use App\Transformers\User\Company\AboutTransformer;
use App\Transformers\User\Company\OfficeTransformer;
use App\Transformers\User\Company\ServiceTransformer;

/**
* 
*/
class CompanyTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'category'
    ];
	
	protected $availableIncludes = [
        'about', 'services', 'teams', 'offices'
    ];

    /**
	* Transform a Company model into an array
	*
	* @param Company $company
	* @return array
	*/
	public function transform(Company $company)
	{
		return [
			'id' => (int) $company->id,
			'companyName' => $company->company_name,
			'taxIdNumber' => $company->tax_id_number,
			'registrationNumber' => $company->registration_number,
			'dateOfIncorporation' => $company->date_of_incorporation,
			'structureType' => $company->structure_type,
			'created' => Carbon::parse($company->created_at)->toIso8601String(),
			'logo' => asset('storage/'. $company->logo),
			'banner' => asset('storage/'. $company->banner),
		];
	}

	public function includeAbout(Company $company)
    {
    	$about = $company->about;

        return $this->item($about, new AboutTransformer);
    }

	public function includeCategory(Company $company)
    {
    	$category = $company->category;

        return $this->item($category, new CategoryTransformer);
    }

	public function includeServices(Company $company)
    {
    	$services = $company->services;

        return $this->collection($services, new ServiceTransformer);
    }

	public function includeTeams(Company $company)
    {
    	$teams = $company->teams;

        return $this->collection($teams, new TeamTransformer);
    }

	public function includeOffices(Company $company)
    {
    	$offices = $company->offices;

        return $this->collection($offices, new OfficeTransformer);
    }
}