<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use Illuminate\Support\Str;
use App\Models\User\Company\Office;
use League\Fractal\TransformerAbstract;
use App\Transformers\User\CompanyTransformer;

/**
* 
*/
class OfficeTransformer extends TransformerAbstract
{
	
	protected $availableIncludes = [
        'company'
    ];

    /**
	* Transform a Office model into an array
	*
	* @param Office $office
	* @return array
	*/
	public function transform(Office $office)
	{
		return [
			'id' => (int) $office->id,
			'country' => Country::find($office->country_id)->name,
			'state' => State::find($office->state_id)->name,
			'city' => City::find($office->city_id)->name,
			'street' => $office->street,
			'email' => $office->email,
			'phone' => $office->phone_number,
			'longitude' => $office->longitude,
			'latitude' => $office->latitude,
			'created' => Carbon::parse($office->created_at)->toIso8601String(),
		];
	}

	public function includeOffice(Office $office)
    {
    	$company = $office->company;

        return $this->item($company, new CompanyTransformer);
    }
}