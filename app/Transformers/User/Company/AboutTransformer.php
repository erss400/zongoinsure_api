<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use Illuminate\Support\Str;
use App\Models\User\Company\About;
use League\Fractal\TransformerAbstract;
use App\Transformers\User\CompanyTransformer;

/**
* 
*/
class AboutTransformer extends TransformerAbstract
{
	
	protected $availableIncludes = [
        'company'
    ];

    /**
	* Transform a About model into an array
	*
	* @param About $about
	* @return array
	*/
	public function transform(About $about)
	{
		return [
			'id' => (int) $about->id,
			'about' => $about->about_us,
			'missionStatement' => $about->mission_statement,
			'emailAddress' => $company->email,
			'phoneNumber' => $company->phone_number,
			'mission' => $about->mission,
			'vision' => $about->vision,
			'created' => Carbon::parse($about->created_at)->toIso8601String(),
		];
	}

	public function includeAbout(About $about)
    {
    	$company = $about->company;

        return $this->item($company, new CompanyTransformer);
    }
}