<?php

namespace App\Transformers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\User\Company\Team;
use League\Fractal\TransformerAbstract;
use App\Transformers\User\CompanyTransformer;

/**
* 
*/
class TeamTransformer extends TransformerAbstract
{
	
	protected $availableIncludes = [
        'company'
    ];

    /**
	* Transform a Team model into an array
	*
	* @param Team $team
	* @return array
	*/
	public function transform(Team $team)
	{
		return [
			'id' => (int) $team->id,
			'name' => $team->name,
			'position' => $team->position,
			'bio' => $team->bio,
			'picture' => asset('storage/'. $team->picture),
			'facebook' => $team->facebook,
			'instagram' => $team->instagram,
			'twitter' => $team->twitter,
			'linkedin' => $team->linkedin,
			'created' => Carbon::parse($team->created_at)->toIso8601String(),
		];
	}

	public function includeTeam(Team $team)
    {
    	$company = $team->company;

        return $this->item($company, new CompanyTransformer);
    }
}