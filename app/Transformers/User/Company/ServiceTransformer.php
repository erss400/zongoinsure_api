<?php

namespace App\Transformers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\User\Company\Service;
use League\Fractal\TransformerAbstract;
use App\Transformers\User\CompanyTransformer;

/**
* 
*/
class ServiceTransformer extends TransformerAbstract
{
	
	protected $availableIncludes = [
        'company'
    ];

    /**
	* Transform a Service model into an array
	*
	* @param Service $service
	* @return array
	*/
	public function transform(Service $service)
	{
		return [
			'id' => (int) $service->id,
			'title' => $service->title,
			'content' => $service->content,
			'picture' => asset('storage/'. $service->picture),
			'policies' => $service->policy,
			'created' => Carbon::parse($service->created_at)->toIso8601String(),
		];
	}

	public function includeService(Service $service)
    {
    	$company = $service->company;

        return $this->item($company, new CompanyTransformer);
    }
}