<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\City;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
	/**
	* Transform a City model into an array
	*
	* @param City $city
	* @return array
	*/
	public function transform(City $city)
	{
		return [
			'id' => (int) $city->id,
			'name' => $city->name,
			'created' => Carbon::parse($city->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($slide->updated_at)->toIso8601String(),
		];
	}
}