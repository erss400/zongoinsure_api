<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
	/**
	* Transform a Country model into an array
	*
	* @param Country $country
	* @return array
	*/
	public function transform(Country $country)
	{
		return [
			'id' => (int) $country->id,
			'name' => $country->name,
			'currency' => $country->currency,
			'currency_code' => $country->currency_code,
			'country_code' => $country->code,
			'capital' => $country->capital,
			'continent_name' => $country->continentName,
			'continent' => $country->continent,
			'languages' => $country->languages,
			'created' => Carbon::parse($country->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($slide->updated_at)->toIso8601String(),
			// 'pic' => asset('storage/'. $country->picture),
		];
	}
}