<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
	/**
     * List of resources to automatically include
     *
     * @var array
     */

	// $defaultIncludes

    protected $availableIncludes = [
        'companies'
    ];

	/**
	* Transform a Category model into an array
	*
	* @param Category $category
	* @return array
	*/
	public function transform(Category $category)
	{
		return [
			'id' => (int) $category->id,
			'title' => $category->title,
			'slug' => $category->slug,
			'description' => $category->description,
			'created' => Carbon::parse($category->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($category->updated_at)->toIso8601String(),
		];
	}

	/**
     * Include Blogs
     *
     * @param Category $category
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCompanies(Category $category)
    {
        $companies = $category->companies;

        return $this->collection($companies, new CompanyTransformer);
    }
}