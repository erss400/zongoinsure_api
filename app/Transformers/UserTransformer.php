<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;
use App\Transformers\User\CompanyTransformer;
use App\Transformers\User\ConsumerTransformer;

class UserTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        
    ];

    protected $availableIncludes = [
    	'consumer', 'company'
    ];

	/**
	* Transform a User model into an array
	*
	* @param User $user
	* @return array
	*/
	public function transform(User $user)
	{
		return [
			'id' => (int) $user->id,
			'name' => $user->name,
			'username' => $user->username,
			'accountType' => $user->account_type,
			'avatar' => asset('storage/'. $user->picture),
			'created' => Carbon::parse($user->created_at)->diffForHumans(),
		];
	}

	public function includeConsumer(User $user)
    {
    	$consumer = $user->consumer;

    	if (empty($consumer)) {
    		return null;
    	}

        return $this->item($consumer, new ConsumerTransformer);
    }

	public function includeCompany(User $user)
    {
    	$company = $user->company;

    	if (empty($company)) {
    		return null;
    	}

        return $this->item($company, new CompanyTransformer);
    }
}