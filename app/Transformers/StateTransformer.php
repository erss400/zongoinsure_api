<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\State;
use League\Fractal\TransformerAbstract;

class StateTransformer extends TransformerAbstract
{
	/**
	* Transform a State model into an array
	*
	* @param State $state
	* @return array
	*/
	public function transform(State $state)
	{
		return [
			'id' => (int) $state->id,
			'name' => $state->name,
			'created' => Carbon::parse($state->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($slide->updated_at)->toIso8601String(),
		];
	}
}