<?php

namespace App\Listeners;

use App\Mail\SignUp;
use App\Events\SignUpEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SignUpListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SignUpEvent  $event
     * @return void
     */
    public function handle(SignUpEvent $event)
    {
        Mail::to($event->user->email)->send(new SignUp($event->user));
    }
}
