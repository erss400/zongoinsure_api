<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'company_name', 'category_id', 'tax_id_number', 'registration_number', 'date_of_incorporation', 'first_name', 'last_name', 'structure_type', 'logo', 'banner', 'password', 'get_started',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the user that owns the company.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the category that owns the company.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get the about for the company.
     */
    public function about()
    {
        return $this->hasOne('App\Models\User\Company\About');
    }

    /**
     * Get the offices for the company.
     */
    public function offices()
    {
        return $this->hasMany('App\Models\User\Company\Office');
    }

    /**
     * Get the services for the company.
     */
    public function services()
    {
        return $this->hasMany('App\Models\User\Company\Service');
    }

    /**
     * Get the teams for the company.
     */
    public function teams()
    {
        return $this->hasMany('App\Models\User\Company\Team');
    }
}
