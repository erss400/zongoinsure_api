<?php

namespace App\Models\User\Company;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    public function company()
    {
    	$this->belongsTo('App\Models\User\Company')
    }
}
