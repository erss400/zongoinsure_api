<?php

namespace App\Models\User\Company;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'position', 'bio', 'picture', 'facebook', 'twitter', 'instagram', 'linkedin'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Models\User\Company');
    }
}
