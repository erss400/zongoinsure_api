<?php

namespace App\Models\User\Company;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'about_us', 'mission_statement', 'mission', 'vision', 'country_id', 'state_id', 'city_id', 'street', 'email', 'phone_number'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Models\User\Company');
    }

    /**
     * Get the country that owns the company.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the state that owns the company.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Get the city that owns the company.
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
