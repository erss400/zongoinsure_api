<?php

namespace App\Models\User\Company;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'title', 'content', 'picture', 'policy'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Models\User\Company');
    }
}
