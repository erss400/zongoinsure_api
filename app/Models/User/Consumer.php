<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_of_birth', 'place_of_birth', 'bio', 'country_id', 'state_id', 'city_id' 'gender'
    ];

    /**
     * Get the user that owns the company.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the country that owns the company.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the state that owns the company.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Get the city that owns the company.
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
