<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function abouts()
    {
    	return $this->hasMany('App\Models\User\Company\About');
    }
}
