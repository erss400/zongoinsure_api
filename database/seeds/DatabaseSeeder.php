<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        $this->call([
        	CountriesTableSeeder::class,
            StatesTableSeeder::class,
            CitiesTableSeeder::class,
            CategoriesTableSeeder::class,
            TypesTableSeeder::class
        ]);
    }
}
