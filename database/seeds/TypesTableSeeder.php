<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('types')->delete();

        \DB::table('types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Alien abduction insurance',
                'slug' => str_slug('Alien abduction insurance'),
                'description' => 'Alien abduction insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Assumption reinsurance',
                'slug' => str_slug('Assumption reinsurance'),
                'description' => 'Assumption reinsurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Aviation insurance',
                'slug' => str_slug('Aviation insurance'),
                'description' => 'Aviation insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Bancassurance',
                'slug' => str_slug('Bancassurance'),
                'description' => 'Bancassurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Boiler insurance',
                'slug' => str_slug('Boiler insurance'),
                'description' => 'Boiler insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Bond insurance',
                'slug' => str_slug('Bond insurance'),
                'description' => 'Bond insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Builder\'s risk insurance',
                'slug' => str_slug('Builder\'s risk insurance'),
                'description' => 'Builder\'s risk insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Business interruption insurance',
                'slug' => str_slug('Business interruption insurance'),
                'description' => 'Business interruption insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Business overhead expense disability insurance',
                'slug' => str_slug('Business overhead expense disability insurance'),
                'description' => 'Business overhead expense disability insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            9 => 
            array (
                'id' => 10,
                'title' => 'Casualty insurance',
                'slug' => str_slug('Casualty insurance'),
                'description' => 'Casualty insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            10 => 
            array (
                'id' => 11,
                'title' => 'Catastrophe bond',
                'slug' => str_slug('Catastrophe bond'),
                'description' => 'Catastrophe bond',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            11 => 
            array (
                'id' => 12,
                'title' => 'Chargeback insurance',
                'slug' => str_slug('Chargeback insurance'),
                'description' => 'Chargeback insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            12 => 
            array (
                'id' => 13,
                'title' => 'Collateral protection insurance',
                'slug' => str_slug('Collateral protection insurance'),
                'description' => 'Collateral protection insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            13 => 
            array (
                'id' => 14,
                'title' => 'Computer insurance',
                'slug' => str_slug('Computer insurance'),
                'description' => 'Computer insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            14 => 
            array (
                'id' => 15,
                'title' => 'Condo insurance',
                'slug' => str_slug('Condo insurance'),
                'description' => 'Condo insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            15 => 
            array (
                'id' => 16,
                'title' => 'Contents insurance',
                'slug' => str_slug('Contents insurance'),
                'description' => 'Contents insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            16 => 
            array (
                'id' => 17,
                'title' => 'Credit insurance',
                'slug' => str_slug('Credit insurance'),
                'description' => 'Credit insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            17 => 
            array (
                'id' => 18,
                'title' => 'Critical illness insurance',
                'slug' => str_slug('Critical illness insurance'),
                'description' => 'Critical illness insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            18 => 
            array (
                'id' => 19,
                'title' => 'Cyber-Insurance',
                'slug' => str_slug('Cyber-Insurance'),
                'description' => 'Cyber-Insurance',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
           ));
    }
}
