@component('mail::message')
# Welocome to {{ env('APP_NAME') }}

Thank You <strong>{{ $data->first_name . ' ' . $data->last_name }}</strong> for your interest to own an account on {{ env('APP_NAME') }}. We appreciate. In other for us to you and our users genuinly you account is under review and we will get back to you within <strong>3</strong> business days with an email informing you if your account has been <strong>APPROVED</strong> or <strong>NOT</strong>

@component('mail::button', ['url' => ''])
Learn more
@endcomponent

Thanks,<br>
{{ env('APP_NAME') }}
@endcomponent
