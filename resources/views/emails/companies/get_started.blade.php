@component('mail::message')
# Get Started

We have reviewed the data you submitted to us and we have <strong>APPROVED</strong> your account.

Your Generated Password: 
Your username: 

<h1>TIPS</h1>
<ol>
	<li>Change password immediate you login the first time</li>
	<li>Provide us your Company's About</li>
	<li>Provide us your Company's Mission</li>
	<li>Provide us your Company's Logo</li>
	<li>Provide us your Company's Mission Statement</li>
	<li>Provide us your Company's Vision</li>
</ol>

@component('mail::button', ['url' => ''])
Get Started Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
