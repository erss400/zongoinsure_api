<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->group(['namespace' => 'App\Http\Controllers\Api\V1'], function ($api)
	{
		$api->get('country/search', 'SearchController@search_country');

		$api->get('state/search', 'SearchController@search_state');

		$api->get('city/search', 'SearchController@search_city');

		$api->get('category/search', 'SearchController@search_category');

		$api->group(['prefix' => 'account'], function ($api)
		{
			$api->post('/register', 'AccountController@register');

			$api->patch('/verify/{code}', 'AccountController@verify');

			$api->post('/login', 'AccountController@login');

			$api->get('/logout', 'AccountController@logout');

			$api->post('/forgot-password', 'AccountController@forgot_password');

			$api->post('/check-reset-password', 'AccountController@check_reset_password_token');

			$api->post('/reset-password', 'AccountController@reset_password');

			$api->get('/me', 'AccountController@me');

			$api->get('/refresh', 'AccountController@refresh');

			$api->patch('/change_password', 'AccountController@change_password');

			$api->patch('/update', 'AccountController@update');

			$api->post('/update_media', 'AccountController@update_picture');

			$api->post('/delete', 'AccountController@delet');

			$api->group(['prefix' => 'company'], function ($api)
			{
				$api->post('/get-started/{user_id}', 'CompanyController@get_started');

				$api->post('/update/{company_id}', 'CompanyController@update');

				$api->post('/update_media/{company_id}', 'CompanyController@update_media');

				$api->post('/update_logo/{company_id}', 'CompanyController@update_logo');

				$api->patch('/add_about/{company_id}', 'CompanyController@add_about');

				$api->patch('/update_about/{company_id}', 'CompanyController@update_about');

				// $api->post('/update_mission', 'CompanyController@update_mission');

				// $api->post('/update-mission-statement', 'CompanyController@update_mission_statement');

				// $api->post('/update-vision', 'CompanyController@update_vision');

				// $api->get('/get_all_service', 'CompanyController@get_all_service');

				// $api->get('/get_all_office', 'CompanyController@get_all_office');

				// $api->get('/get_all_team', 'CompanyController@get_all_team');

				$api->post('/add_service/{company_id}', 'CompanyController@add_service');

				$api->post('/update_service', 'CompanyController@update_service');

				$api->delete('/delete_service', 'CompanyController@delete_service');

				$api->patch('/add_office/{company_id}', 'CompanyController@add_office');

				$api->post('/update_office', 'CompanyController@update_office');

				$api->delete('/delete_office', 'CompanyController@delete_office');

				$api->post('/add_team/{company_id}', 'CompanyController@add_team');

				$api->post('/update_team', 'CompanyController@update_team');

				$api->delete('/delete_team', 'CompanyController@delete_team');

				$api->delete('delete', 'CompanyController@delete');
			});
		});
	});
});